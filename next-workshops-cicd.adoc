For a DevOps Automator and QA engineers we plan to release:

- Continuous Integration, and Continuous Delivery/Deployment for project using microservices architecture with a help of the GITLAB platform. Delivery/Deployment will be targeting prepared K8s cluster.

Current agenda for the first edition looks like this:

* Start 10:00
* Section 1:
** Architecture Overview
** Gitlab Overview
** Frontend Service Pipeline Overview
** Backend Services Pipelines Overview
** Gitlab Container Registry Overview
** Gitlab Pages Overview
** Gitlab CI Overview
* Coffee break 11:00 - 11:15
* Section 2:
** Your first pipeline (practice + explanation)
** A complex pipeline (practice + explanation)
* Coffee break 12:10 - 12:25
* Section 3:
** DevOps pipeline for our frontend microservice (demo)
** DevOps pipeline for our frontend microservice (explanation)
* Section 4:
** DevOps pipeline for our backend microservices (demo)
** DevOps pipeline for our backend microservices (explanation)
* Lunch 13:45 - 14:15
* Section 5:
** Simplified pipeline for our frontend app (explanation)
** Simplified pipeline for our frontend app (practice)
* Coffee break 15:45 - 16:00
* Section 6:
** What's Next
** Cleanup
** Q&A
* Ends between 16:15 and 16:30 - depends on Q&A session