[.lead]
Now finally all elements of ours services local deployment are explain, and we can practice it by deploying all our microservices to the prepared K3s environment.

We just need to create `services` namespace firs before this procedure with the command:

[source,shell]
----
kubectl create namespace services
----

And we can also switch from the `default` namespace to the `services` namespace globally for easy access in the `k9s`:

[source,shell]
----
kubens services
----

[IMPORTANT]
You can find Ansible playbook password in each project `README.md` file
where it **SHOULD NOT BE !!!**. This password is: `ThisIsExamplePassword4U`. You will be using this password a lot in the next sections.

We also need to have our helm repository registered if we didn't do this already:

[source,shell]
----
helm repo add ex-book https://gitlab.com/api/v4/projects/39005882/packages/helm/stable
----

[WARNING]
====
THIS STEP IS REQUIRED TO BE DONE ONCE DURING THE TRAINING!
====

After registering our example public helm repository we just need to update our helm repository list

[source,shell]
----
helm repo update
----
