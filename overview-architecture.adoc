Our target backend architecture will be looking like this:

ifdef::backend-html5[]
.Target System Architecture
[#img-system]
[caption="Figure 3: "]
image::images/system.svg[Target Architecture]
endif::[]
ifndef::backend-html5[]
.Target System Architecture
[#img-system]
[caption="Figure 3: "]
image::images/system.png[Target Architecture]
endif::[]

- Our FrontEnd Service will be simulated with https://gitlab.com/mobica-workshops/examples/js/vuejs/book-frontend[Book Frontend].

We have three backend microservices prepared which we will deploy to make our Frontend application working:

- Our BFF Service will be simulated with https://gitlab.com/mobica-workshops/examples/go/gorilla/book-bff[Book API Gateway Service].
- Our API-1 Service will be simulated with https://gitlab.com/mobica-workshops/examples/go/gorilla/book-list[Book List API Service].
- Our API-2 Service will be simulated with https://gitlab.com/mobica-workshops/examples/go/gorilla/book-admin[Book Admin API service]

In the upper target system which will be available on the staging and production environments our frontend is talking through the Ingres with our BFF Service which is operating in the fully working environment.

Locally we will be using K3s cluster, and we will resign from Ingress to not overcomplicate our helm chart with different Ingress setup.
We will just use NodePort configuration to simply it.

