We will be installing Portainerfootnote:[Portainer https://docs.portainer.io/] with the help of helm tools.

First we need to add portainer repository to our system.

[source,shell]
----
helm repo add portainer https://portainer.github.io/k8s/
helm repo update
----

[IMPORTANT]
====
We will be talking more about helm repositories later when we will be analyzing our example projects helm chart.
====

[NOTE]
====
We need to use `helm repo add portainer https://portainer.github.io/k8s/` only once in our system. We will be updating this repo with `helm repo update portainer` command when there will be new version of this chart available. Or like in the upper example just update all repos we have.
====

Now we can install portainer with this command:

[source,shell]
----
helm install --create-namespace -n portainer portainer portainer/portainer
----

[IMPORTANT]
====
We will be focusing on helm commands in the next chapter
====

Which will have similar output to this:

[source,text]
----
NAME: portainer
LAST DEPLOYED: Mon Jan 15 22:35:21 2024
NAMESPACE: portainer
STATUS: deployed
REVISION: 1
NOTES:
Get the application URL by running these commands:
    export NODE_PORT=$(kubectl get --namespace portainer -o jsonpath="{.spec.ports[1].nodePort}" services portainer)
  export NODE_IP=$(kubectl get nodes --namespace portainer -o jsonpath="{.items[0].status.addresses[0].address}")
  echo https://$NODE_IP:$NODE_PORT
----

Let's check status of our deployment:

[source,shell]
----
kubectl get pods -n portainer
----

Which will have similar output to this:

[source,text]
----
NAME                        READY   STATUS    RESTARTS   AGE
portainer-b4d95987c-bm64g   1/1     Running   0          2m3s
----

you must wait till STATUS will be Running and READY will be 1/1

Our service is running but as Portainer needs to be restarted at least once we need to remove this pod:

[source,shell]
----
kubectl delete pod portainer-b4d95987c-bm64g -n portainer
----

and wait till it will be `Running` again.

Visit page: http://localhost:30777/

You should see something like this:

image::images/portainer-create-admin-user.png[Welcome Page]

Please give password to your admin user and you will be redirected to the another welcome page:

image::images/portainer-welcome-page.png[Create Admin User page]

Just click on the `Get Started` box. This will create local environment we need:

image::images/portainer-home.png[Home page]

Let's go to this `local` environment by clicking on the local box:

image::images/portainer-local.png[Local environment page]

Let's go to the `Applications` page:

image::images/portainer-applications-list-empty.png[Applications page]

We will create our first application there. Choose button `+ Create from manifest` and in the new form set `name` to be `nginx` and choose `Build method` to be `Web editor`

image::images/portainer-create-app-part-1.png[Create Application - part 1]

In the Web Editor part of the form we will put this Kubernetes manifest:

[source,yaml]
----
---
apiVersion: v1
kind: Pod
metadata:
  name: nginx
  labels:
    app.kubernetes.io/name: proxy
spec:
  containers:
    - name: nginx
      image: nginx:stable
      ports:
        - containerPort: 80
          name: http-web-svc

---
apiVersion: v1
kind: Service
metadata:
  name: nginx-service
spec:
  type: NodePort
  selector:
    app.kubernetes.io/name: proxy
  ports:
    - name: name-of-service-port
      protocol: TCP
      port: 80
      targetPort: http-web-svc
      nodePort: 30700
----

and we need to click Deploy button:

image::images/portainer-create-app-part-2.png[Create Application - part 2]

[NOTE]
====
This is exactly the same result we will have by just saving this manifest file as for example `myApp.yml` and running `kubectl apply -f myApp.yml` ;).
====

We just need to refresh Application page till status will be `Running` and check if all work by visiting page: http://localhost:30700/ .

