To test if our service is ready for the deployment to the Kubernetes cluster we will use our local K3s installation.

[WARNING]
====
PLEASE BE SURE THAT YOU REMOVED bookCluster CREATED IN THE OTHER TRAININGS AS THIS ONE IS MORE COMPLEX
====

Please use this command to create our **K3s** cluster:

[source,shell]
----
k3d cluster create bookCluster --api-port 6443 --servers 1 --agents 3 -p "30700-30799:30700-30799@server:0"
----

You should have similar output to this one:

[source,text]
----
INFO[0000] Prep: Network
INFO[0000] Created network 'k3d-bookCluster'
INFO[0000] Created image volume k3d-bookCluster-images
INFO[0000] Starting new tools node...
INFO[0000] Starting Node 'k3d-bookCluster-tools'
INFO[0001] Creating node 'k3d-bookCluster-server-0'
INFO[0001] Creating node 'k3d-bookCluster-agent-0'
INFO[0001] Creating node 'k3d-bookCluster-agent-1'
INFO[0001] Creating node 'k3d-bookCluster-agent-2'
INFO[0001] Creating LoadBalancer 'k3d-bookCluster-serverlb'
INFO[0001] Using the k3d-tools node to gather environment information
INFO[0001] Starting new tools node...
INFO[0001] Starting Node 'k3d-bookCluster-tools'
INFO[0002] Starting cluster 'bookCluster'
INFO[0002] Starting servers...
INFO[0002] Starting Node 'k3d-bookCluster-server-0'
INFO[0005] Starting agents...
INFO[0005] Starting Node 'k3d-bookCluster-agent-1'
INFO[0005] Starting Node 'k3d-bookCluster-agent-2'
INFO[0005] Starting Node 'k3d-bookCluster-agent-0'
INFO[0008] Starting helpers...
INFO[0008] Starting Node 'k3d-bookCluster-serverlb'
INFO[0015] Injecting records for hostAliases (incl. host.k3d.internal) and for 6 network members into CoreDNS configmap...
INFO[0017] Cluster 'bookCluster' created successfully!
INFO[0017] You can now use it like this:
kubectl cluster-info
----


Then you can check it with this command:

[source,shell]
----
kubectl cluster-info
----

And you will have similar output to this:

[source,text]
----
Kubernetes control plane is running at https://0.0.0.0:6443
CoreDNS is running at https://0.0.0.0:6443/api/v1/namespaces/kube-system/services/kube-dns:dns/proxy
Metrics-server is running at https://0.0.0.0:6443/api/v1/namespaces/kube-system/services/https:metrics-server:https/proxy

To further debug and diagnose cluster problems, use 'kubectl cluster-info dump'.
----

let's analise our creation command a little and our cluster itself. But before it to better understand it let start with some definitions:

----
A server node is defined as a host running the k3s server command, with control-plane and datastore components managed by K3s. An agent node is defined as a host running the k3s agent command, without any datastore or control-plane components. Both servers and agents run the kubelet, container runtime, and CNI.
----

Starting with command:

- `k3d cluster create bookCluster` - this command will create cluster with the name bookCluster and other parameters given after it will just help to create it in a way we wanted it to be created
- `--api-port 6443` - we are letting know to serve API on the standard port with will be used by our Portainer installation later. You can see this with `kubectl cluster-info` command.
- `--servers 1` - we are creating one server node
- `--agents 3` - we are creating three agent nodes
- `-p "30700-30799:30700-30799@server:0"` we are forwarding ports from 30700 to 30799 to the same set of ports in the server node. This is used by our NodePort setup later and Portainer we will install.

Let's see our nodes in details:

[source,shell]
----
kubectl get nodes
----

Which will have similar output to this:

[source,text]
----
NAME                       STATUS   ROLES                  AGE   VERSION
k3d-bookcluster-server-0   Ready    control-plane,master   15m   v1.27.4+k3s1
k3d-bookcluster-agent-0    Ready    <none>                 15m   v1.27.4+k3s1
k3d-bookcluster-agent-1    Ready    <none>                 15m   v1.27.4+k3s1
k3d-bookcluster-agent-2    Ready    <none>                 15m   v1.27.4+k3s1
----

Now let's check how this look in docker:

[source,shell]
----
docker ps
----

Which will have similar output to this:

[source,text]
----
CONTAINER ID   IMAGE                            COMMAND                  CREATED          STATUS          PORTS                                                                  NAMES
21b4482a9c92   ghcr.io/k3d-io/k3d-tools:5.6.0   "/app/k3d-tools noop"    18 minutes ago   Up 18 minutes                                                                          k3d-bookCluster-tools
4155361e53ff   ghcr.io/k3d-io/k3d-proxy:5.6.0   "/bin/sh -c nginx-pr…"   18 minutes ago   Up 18 minutes   0.0.0.0:6443->6443/tcp, 80/tcp, 0.0.0.0:30700-30799->30700-30799/tcp   k3d-bookCluster-serverlb
8a402b085668   rancher/k3s:v1.27.4-k3s1         "/bin/k3d-entrypoint…"   18 minutes ago   Up 18 minutes                                                                          k3d-bookCluster-agent-2
c4df3fe3e02a   rancher/k3s:v1.27.4-k3s1         "/bin/k3d-entrypoint…"   18 minutes ago   Up 18 minutes                                                                          k3d-bookCluster-agent-1
16b87a3c4b9e   rancher/k3s:v1.27.4-k3s1         "/bin/k3d-entrypoint…"   18 minutes ago   Up 18 minutes                                                                          k3d-bookCluster-agent-0
1d19f2f709f6   rancher/k3s:v1.27.4-k3s1         "/bin/k3d-entrypoint…"   18 minutes ago   Up 18 minutes                                                                          k3d-bookCluster-server-0
----

Like you can see we have also special LB node `k3d-bookCluster-serverlb` and tools node `k3d-bookCluster-tools`.

[NOTE]
====
You can always stop this cluster to save resources with:

[source,shell]
----
k3d cluster stop bookCluster
----

And start it again with:

[source,shell]
----
k3d cluster start bookCluster
----

Because you can have multiple clusters you can always change to this one with command like this:

[source,shell]
----
kubectx k3d-bookCluster
----
====
