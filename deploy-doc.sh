#!/usr/bin/env bash
mkdir public
cp -R images public/
cp microservices-infrastructure-development-guide.html public/index.html
cp microservices-infrastructure-development-guide.pdf public/
