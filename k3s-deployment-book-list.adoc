Please clone our `book-list` project available here: https://gitlab.com/mobica-workshops/examples/go/gorilla/book-list to the location you like to store our example services:

[source,shell]
----
git clone https://gitlab.com/mobica-workshops/examples/go/gorilla/book-list.git
cd book-list
----

Now we need to generate our service configuration and helm values:

[source,shell]
----
./local-configure.sh
----

With required files generated we can now deploy our service to the K3s

[source,shell]
----
./deploy-k3s.sh
----

We can see our deployment status with portainer or k9s:

[source,shell]
----
k9s -n services
----

After successful deployment we can analyze what this shell script we used do:

[source,bash]
----
#!/usr/bin/env bash
set -e
kubectx k3d-bookCluster # <.>
./build-multi-stage-container.sh # <.>
docker pull postgres:14-alpine # <.>
k3d image import -c bookCluster book-list:latest # <.>
k3d image import -c bookCluster postgres:14-alpine # <.>
helm upgrade -i -f secrets/k3s-values.yaml --namespace=services k3s-book-list ex-book/book-service --version 0.6.1 --dry-run --debug # <.>
helm upgrade -i -f secrets/k3s-values.yaml --namespace=services k3s-book-list ex-book/book-service --version 0.6.1 --wait # <.>
----
<.> We are switching to the `k3d-bookCluster` to be sure we will not for accident deploy to the different cluster we plan to
<.> We are building our service with the multi-stage docker container explained on the Backend Workshops.
<.> We are ensuring that we have used by this service postgres:14-alpine image locally as we will push it to the cluster to speed up our deployment process.
<.> We are importing to our k3s cluster created moment ago image of our service as we will be using it in the deployment manifest.
<.> We are importing our cached `postgres:14-alpine` image to our cluster
<.> We are running our helm install/upgrade command with a `--dry-run --debug` mode for visualisation purposes - normally this is not need as you can use helm commands to find all this data for your deployment.
<.> And in the end we are properly deploying our service with the `--wait` to wait for the process to finish with a success or failure. Without this it is fire and forgot type of the command.

For the purpose of testing in this project we have the `docker-compose-k3s.yml` file which we will now put to use:

[source,shell]
----
docker-compose -f docker-compose-k3s.yml up
----

When all will finish starting you can just go to the browser and open page http://localhost:19010

Now switch server to `K3d API endpoint` and try health. You should have in the response:

[source,json]
----
{
  "status": 200,
  "message": "book-list api health",
  "content": {
    "alive": true,
    "postgres": true
  }
}
----

You can also use `curl` instead of the browser:

[source,shell]
----
curl -X 'GET' \
  'http://localhost:30782/v1/health' \
  -H 'accept: application/json'
----

[NOTE]
====
You can play with other endpoints if you like
====

Now because for this demo we started this service with CORS enabled and other services will be very unhappy later we need to stop our service to add CORS. To achieve this we need to modify by hand our generated `secrets/k3s-values.yaml` file.

Search for line looking like this:

[source,yaml]
----
argsOverride: ["serve", "--config", "/secrets/local.env.yaml", "-b", "0.0.0.0", "-p", "8080", "-m", "-w", "10", "-l", "-c"]
----

and change it to look like this:

[source,yaml]
----
argsOverride: ["serve", "--config", "/secrets/local.env.yaml", "-b", "0.0.0.0", "-p", "8080", "-m", "-w", "10", "-l"]
----

and we need to deploy again with the use of our deployment script:

[source,shell]
----
./deploy-k3s.sh
----

To be sure that all was done correct just go to the http://localhost:19010 page and try to use this health endpoint again. You should have:

[source,text]
----
CORS
Network Failure
URL scheme must be "http" or "https" for CORS request.
----

and curl should continue to work properly.
