[.lead]
Terraform is a leading tool for the Infrastructure as Code [IaC]. To better understand how it works we will start with something very simple which we can check ourselves just now.

We will start with simple preparations by creating `prometheus` namespace, and we will deploy this system not using helm directly but by using `helm` provider for the `terraform`.

[source,shell]
----
kubectl create namespace prometheus
----

You can monitor this process with k9s which we will start in the separate terminal tab:

[source,shell]
----
k9s -n prometheus
----

Please create folder `prometheus` and create file `main.tf` looking like this inside:

[source,hcl-terraform]
----
terraform {
  required_version = "~> 1"
  required_providers {
    helm = {
      source  = "hashicorp/helm"
      version = "~> 2"
    }
  }
}

provider "helm" {
  kubernetes {
    config_path = "~/.kube/config"
  }
}

variable "chart_version" {
  default = "48.3.3"
  description = "Helm chart version: https://github.com/prometheus-community/helm-charts/blob/main/charts/kube-prometheus-stack/Chart.yaml"
}

resource "helm_release" "prometheus" {
  name       = "prometheus"
  namespace  = "prometheus"
  repository = "https://prometheus-community.github.io/helm-charts"
  chart      = "kube-prometheus-stack"
  version    = var.chart_version
}
----

Let us start with the `terraform` block:

[source,hcl-terraform]
----
terraform {
  required_version = "~> 1" # <.>
  required_providers { # <.>
    helm = { # <.>
      source  = "hashicorp/helm" # <.>
      version = "~> 2" # <.>
    }
  }
}
----
<.> In our example only restriction to the used version of terraform is that it need to be version 1.x. If you will be using like me `tfswitch`app it will search for this and use or download and use the latest version of terraform 1.x.
<.> We are letting know to terraform which providers and in which versions will be used.
<.> We are naming provider we are using in this section
<.> Here we are informing terraform about source of the provider to use
<.> Here we are informing terraform about version of the provider to use

As you can see in summary we will be using terraform in the version 1.x and also `hashicorp/helm` provider in the version 2.x

Each provider also can have own configuration.

[source,hcl-terraform]
----
provider "helm" {
  kubernetes { # <.>
    config_path = "~/.kube/config" # <.>
  }
}
----
<.> Helm provider can have kubernetes configuration block. You should check official documentation of this provider to learn more.
<.> In our example we are just showing where kube configuration file is located. It is not the best method of doing this as we are not specifying which cluster to use.

We can also have `variables` in help like here:

[source,hcl-terraform]
----
variable "chart_version" { # <.>
  default = "48.3.3" # <.>
  description = "Helm chart version: https://github.com/prometheus-community/helm-charts/blob/main/charts/kube-prometheus-stack/Chart.yaml" # <.>
}
----
<.> We are naming our variable here.
<.> We can specify default value.
<.> We can specify description of the value.

In many cases you should also specify type of the value but here we are showing type by adding default.


[source,hcl-terraform]
----
resource "helm_release" "prometheus" { # <.>
  name       = "prometheus" # <.>
  namespace  = "prometheus" # <.>
  repository = "https://prometheus-community.github.io/helm-charts" # <.>
  chart      = "kube-prometheus-stack" # <.>
  version    = var.chart_version # <.>
}
----
<.> Resource also needs to be named, and it has a format like this `resource "<provider>_<resource_type>" "<internal_resource_name>" {'.
<.> In our case we will name our helm release: `prometheus`.
<.> In our case we will install our package in the namespace: `prometheus`.
<.> We are showing which helm repository will be used here.
<.> We are informing which chart should be used from the set upper repository.
<.> We are also choosing which version to use and in this example we are using variable here.


Now after our explanation move to the `prometheus` folder when we have upper file in your main terminal tab, and if you are using `tfswitch` run it:


[source,shell]
----
tfswitch
----

In my case I had this output:

[source,text]
----
Reading required version from terraform file
Reading required version from constraint: ~> 1
Matched version: 1.7.0
Installing terraform at /Users/2300441/bin
Downloading to: /Users/2300441/.terraform.versions
25891366 bytes downloaded
Switched terraform to version "1.7.0"
----
It can differ if you are starting this app first time.

Next action you need to do when first time doing this in each folder you have is:

[source,shell]
----
terraform init
----

[source,text]
----
Initializing the backend...

Initializing provider plugins...
- Finding hashicorp/helm versions matching "~> 2.0"...
- Installing hashicorp/helm v2.12.1...
- Installed hashicorp/helm v2.12.1 (signed by HashiCorp)

Terraform has created a lock file .terraform.lock.hcl to record the provider
selections it made above. Include this file in your version control repository
so that Terraform can guarantee to make the same selections by default when
you run "terraform init" in the future.

Terraform has been successfully initialized!

You may now begin working with Terraform. Try running "terraform plan" to see
any changes that are required for your infrastructure. All Terraform commands
should now work.

If you ever set or change modules or backend configuration for Terraform,
rerun this command to reinitialize your working directory. If you forget, other
commands will detect it and remind you to do so if necessary.
----

It will check what your terraform setup requires and download it when needed. In this part also state is used or created.

Now we can check what exactly terraform is planning to do:

[source,shell]
----
terraform plan
----

[source,text]
----
Terraform used the selected providers to generate the following execution plan. Resource actions are indicated with the following
symbols:
  + create

Terraform will perform the following actions:

  # helm_release.prometheus will be created
  + resource "helm_release" "prometheus" {
      + atomic                     = false
      + chart                      = "kube-prometheus-stack"
      + cleanup_on_fail            = false
      + create_namespace           = false
      + dependency_update          = false
      + disable_crd_hooks          = false
      + disable_openapi_validation = false
      + disable_webhooks           = false
      + force_update               = false
      + id                         = (known after apply)
      + lint                       = false
      + manifest                   = (known after apply)
      + max_history                = 0
      + metadata                   = (known after apply)
      + name                       = "prometheus"
      + namespace                  = "default"
      + pass_credentials           = false
      + recreate_pods              = false
      + render_subchart_notes      = true
      + replace                    = false
      + repository                 = "https://prometheus-community.github.io/helm-charts"
      + reset_values               = false
      + reuse_values               = false
      + skip_crds                  = false
      + status                     = "deployed"
      + timeout                    = 300
      + verify                     = false
      + version                    = "48.3.3"
      + wait                       = true
      + wait_for_jobs              = false
    }

Plan: 1 to add, 0 to change, 0 to destroy.

───────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────

Note: You didn't use the -out option to save this plan, so Terraform can't guarantee to take exactly these actions if you run
"terraform apply" now.
----

After checking our plan we will apply those changes. Please read `Note` upper suggesting better approach. Please after checking plan again write `yes` and press kbd:[Enter].

[source,shell]
----
terraform apply
----

You can observe now process of applying, and in the main tab where terraform is running you will see something like this:

[source,text]
----
... redacted ...

Do you want to perform these actions?
  Terraform will perform the actions described above.
  Only 'yes' will be accepted to approve.

  Enter a value: yes

helm_release.prometheus: Creating...
helm_release.prometheus: Still creating... [10s elapsed]
helm_release.prometheus: Still creating... [20s elapsed]
helm_release.prometheus: Still creating... [30s elapsed]
helm_release.prometheus: Still creating... [40s elapsed]
helm_release.prometheus: Still creating... [50s elapsed]
helm_release.prometheus: Still creating... [1m0s elapsed]
helm_release.prometheus: Creation complete after 1m1s [id=prometheus]

Apply complete! Resources: 1 added, 0 changed, 0 destroyed.
----

In my case after some time I started to see statistics in the Lens:

image::images/iac-terraform-helm-lens.png[]
