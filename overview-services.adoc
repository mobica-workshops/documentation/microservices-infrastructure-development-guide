Our BackEnd services are created using Gofootnote:[Go Language https://go.dev/] Language.

-   For a CLI part of our application Cobrafootnote:[Cobra library https://github.com/spf13/cobra] was used.
-   For a configuration management part Viperfootnote:[Viper library https://github.com/spf13/viper] was used.
-   For a REST service part of services Gorillafootnote:[Gorilla web toolkit https://www.gorillatoolkit.org/] web toolkit was used.
-   For logging Logrusfootnote:[Logrus https://github.com/sirupsen/logrus] library was used.

You can check what commands and options each service have by using `--help` when running generated binary file.
