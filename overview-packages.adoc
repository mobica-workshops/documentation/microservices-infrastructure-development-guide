Helm is well known as the package manager for Kubernetes. We will be using it to deploy portainer to our cluster like also our own simple nginx based application and our example services. We will be also using special help provider for the Terraform to show how to use helm charts with the Terraform.

Our example services are using one specially prepared helm package available here:
https://gitlab.com/mobica-workshops/examples/helm/book-service

[NOTE]
====
In most of the examples in the internet and also by Helm designers our flow shown in this project is abnormal.
Helm was designed based on other package managers like for example apt.
====

Our helm chart was created with the help of `helm create NAME` command and then improved to fit our needs. It is a little more complex because is used for all services we have in the project but because of this we do not need to maintain helm chart per service which is our main reason to go against standard flow.

We will be creating our own simple chart during this workshop and deploying with a help of it and also analyzing our services chart used for our examples like mentioned upper.
