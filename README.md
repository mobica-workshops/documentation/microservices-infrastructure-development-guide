# Developing microservices - Infrastructure

## Checklist v1.0

- [x] Azure core infrastructure
- [x] AWS core infrastructure
- [x] GCP core infrastructure
- [x] GKE cluster
- [x] Requirements section
- [X] Terraform playbook code - GCP
- [X] Terraform modules code - GCP
- [X] Agenda section
- [X] Microservices Overview section
- [X] K3s section
- [X] Portainer section
- [X] Helm section (practice)
- [X] Ansible section (services download and configuring with explanation + practice)
- [X] Services deployment to K3s section (practice)
- [X] Terraform (GCP example)
- [X] Terraform (Helm provider practice)

## Checklist v1.1

- [ ] https://opentofu.org/ - free Terraform clone
- [ ] Terraform playbook code - Azure
- [ ] Terraform modules code - Azure
- [ ] Terraform playbook code - AWS
- [ ] Terraform modules code - AWS
- [ ] Adapt testing examples from the Terraform up and Running which are using https://terratest.gruntwork.io/
- [ ] terraform validate in the CI
- [ ] check [Checkov](https://www.checkov.io/)
- [ ] check [terraform-compliance](https://terraform-compliance.com/)
- [ ] https://www.pulumi.com/ example
- [ ] ... to be specified later
